package com.example.gatewayexample;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiGatewayConfiguration {

    @Bean
    public RouteLocator gatewayRouter(RouteLocatorBuilder builder){
        return builder.routes()
                .route(p -> p.path("/get")
                        .filters(f -> f
                                .addRequestHeader("MyHeader","MyURI")
                                .addRequestParameter("Param","MyValue"))
                        .uri("http://httpbin.org:80"))
                .route(p -> p.path("/cases/**")
                        .uri("http://localhost:8081/"))
                .route(p -> p.path("/authenticate/**")
                        .uri("http://localhost:8082/"))
                .route(p -> p.path("/users/**")
                        .uri("http://localhost:8082"))
                .route(p -> p.path("/user/**")
                        .uri("http://localhost:8082"))
                .build();
    }
}
