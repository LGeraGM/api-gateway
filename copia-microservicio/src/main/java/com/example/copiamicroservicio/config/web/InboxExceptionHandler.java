package com.example.copiamicroservicio.config.web;

import com.example.copiamicroservicio.exception.CaseNotFoundException;
import com.example.copiamicroservicio.exception.UserNotFoundException;
import com.example.copiamicroservicio.model.DefaultError;
import com.example.copiamicroservicio.model.DefaultErrorList;
import com.example.copiamicroservicio.util.ErrorEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * @author David Gonzalez
 * 
 * Esta clase se encarga de servir como apoyo al controller, manejando de manera desacoplada 
 * las excepciones esperadas en la aplicacion, y manejando el catalogo de errores con ayuda de un enumerador personalizado.
 * Tambien tiene un manejo de errores genericos.
 */
@ControllerAdvice
public class InboxExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(InboxExceptionHandler.class);
    
    /**
     * Manejo de excepciones de validacion de argumentos de entrada
     * @param pe Excepcion de tipo MethodArgumentNotValidException
     * @return La entidad de respuesta que maneja el error como objeto
     */
	@ExceptionHandler(value = {MethodArgumentNotValidException.class})
	public ResponseEntity<DefaultErrorList> handleValidationExceptionA(MethodArgumentNotValidException pe) {
    	LOGGER.warn("Argmentos invalidos", pe);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_ERROR_PARAMS, pe.getLocalizedMessage())), HttpStatus.BAD_REQUEST);

	}

	/**
     * Manejo de excepciones de validacion de formatos de numeros de entrada
     * @param pe Excepcion de tipo NumberFormatException
     * @return La entidad de respuesta que maneja el error como objeto
     */
	@ExceptionHandler(value = {NumberFormatException.class})
	public ResponseEntity<DefaultErrorList> handleValidationExceptionB(NumberFormatException pe) {
    	LOGGER.warn("Excepcion de formatos de numeros de entrada", pe);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_ERROR_PARAMS, pe.getLocalizedMessage())), HttpStatus.BAD_REQUEST);

	}

	/**
     * Manejo de excepciones de validacion de tipo de datos de entrada
     * @param pe Excepcion de tipo MethodArgumentTypeMismatchException
     * @return La entidad de respuesta que maneja el error como objeto
     */
	@ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
	public ResponseEntity<DefaultErrorList> handleValidationExceptionC(MethodArgumentTypeMismatchException pe) {
    	LOGGER.warn("Excepcion de tipo de datos de entrada", pe);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_ERROR_PARAMS, pe.getLocalizedMessage())), HttpStatus.BAD_REQUEST);

	}


	/**
     * Manejo de excepciones de validacion de cantidad de parametros de entrada enviados
     * @param pe Excepcion de tipo MissingServletRequestParameterException
     * @return La entidad de respuesta que maneja el error como objeto
     */
	@ExceptionHandler(value = {MissingServletRequestParameterException.class})
	public ResponseEntity<DefaultErrorList> handleValidationExceptionD(MissingServletRequestParameterException pe) {
    	LOGGER.warn("Excepcion de cantidad de parametros de entrada enviados", pe);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_ERROR_PARAMS, pe.getLocalizedMessage())), HttpStatus.BAD_REQUEST);
	}
/**
 * Manejo de excepciones de usuario no encontrado y/o valido
 * @param unf
 * @return
 */
    @ExceptionHandler(value= {UserNotFoundException.class})
    public ResponseEntity<DefaultErrorList> handleUserException(UserNotFoundException unf){
		LOGGER.error("Excepcion de usuario no encontrado", unf);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_USER_NOT_FOUND, unf.getLocalizedMessage())), HttpStatus.UNAUTHORIZED);
    }
	/**
	 * Manejo de Excepciones de caso no valido y/o encontrado
	 * @param cnf
	 * @return
	 */
    @ExceptionHandler(CaseNotFoundException.class)
    public ResponseEntity<DefaultErrorList> handleCaseException(CaseNotFoundException cnf){
    	LOGGER.error("Excepccion de caso no valido");
    	return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_CASE_NOT_FOUND, cnf.getLocalizedMessage())),HttpStatus.NOT_FOUND);
    }
    
	/**
	 * Manejo de excepcion generica
	 * @param ex Excepcion generica de tipo Exception
	 * @return La entidad de respuesta que maneja el error como objeto
	 */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<DefaultErrorList> handleGenericException(Exception ex) {
		LOGGER.error("Error en la ejecucion", ex);
		return new ResponseEntity<>(new DefaultErrorList(new DefaultError(ErrorEnum.EXC_GENERICO)), HttpStatus.INTERNAL_SERVER_ERROR);
    }

	
}
