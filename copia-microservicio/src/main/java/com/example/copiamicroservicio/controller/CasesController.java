/**
 * ********************************
 * Project: Case Management
 *
 * @author Gerardo Garcia
 * Empresa: Minsait
 * Fecha: 21/01/20222
 * TIPO: Controller
 * *********************************
 */
package com.example.copiamicroservicio.controller;

import java.util.List;
import java.util.stream.Collectors;

import com.example.copiamicroservicio.config.security.JwtUtil;
import com.example.copiamicroservicio.entity.CaseAssig;
import com.example.copiamicroservicio.exception.CaseNotFoundException;
import com.example.copiamicroservicio.exception.UserNotFoundException;
import com.example.copiamicroservicio.model.CaseAssigDTO;
import com.example.copiamicroservicio.service.ICaseService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * Minsait Indra Mexico Clase: CaseController.java Descriptcion: Controlador que
 * responde a los cambios desde la interfaz. Control de Cambios: 1.0 Dic 21,
 * 2022 Gerardo - Creacion
 */
@Slf4j
@RestController
@CrossOrigin(value="http://localhost:4200")
@RequestMapping("/cases")
public class CasesController {
	@Autowired
	private ModelMapper modelMapper;

	// Instancia con el servicio
	@Autowired
	private ICaseService caseService;

	@Autowired
	JwtUtil jwtUtil;

	/**
	 * Controller para mostrar los casos del usuario en sesion
	 * 
	 * @throws CaseNotFoundException
	 * 
	 * @url: /cases
	 */
	@ApiOperation(value = "Casos especificos de usuario")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Resultado correcto"),
			@ApiResponse(code = 404, message = "Resultado no encontrado") })
	@GetMapping
	public List<CaseAssigDTO> getAllCasesByUser(@RequestHeader("Authorization") String token)
			throws UserNotFoundException, CaseNotFoundException {
		log.info("Controller, obtener todos los caso del usuario en sesion");
		// Llamado de servicio para obtener los casos del usuario en sesion (utilizando
		// DTO)
		return caseService.getAllCasesByUser(token).stream()
				.map(caseAssig -> modelMapper.map(caseAssig, CaseAssigDTO.class)).collect(Collectors.toList());
	}

	/**
	 * Controller para mostrar un caso especifico
	 * 
	 * @param token
	 * @param idCase
	 * @return
	 * @url: /cases/{idCase}
	 */
	@ApiOperation(value = "Caso especifico de usuario")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Resultado correcto"),
			@ApiResponse(code = 404, message = "Resultado no encontrado") })
	@GetMapping("/{idCase}")
	public ResponseEntity<CaseAssigDTO> getCaseById(@RequestHeader("Authorization") String token,
			@PathVariable Integer idCase) throws UserNotFoundException, CaseNotFoundException {
		CaseAssig caseAssig = caseService.getCaseById(token, idCase);

		CaseAssigDTO caseResponse = modelMapper.map(caseAssig, CaseAssigDTO.class);
		return ResponseEntity.ok().body(caseResponse);
	}

	/**
	 * Controller para insertar nuevos casos
	 * 
	 * @url: /cases}
	 * @param: con el token se obtiene el id del usuario y RequestBody con la
	 *             estructura qur tienen los casos (entidad CaseAssig)
	 * @throws CaseNotFoundException
	 */

	@ApiOperation(value = "Insercion de nuevo caso")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Caso creado"),
			@ApiResponse(code = 404, message = "Caso no encontrado") })
	@PostMapping
	public ResponseEntity<CaseAssigDTO> createCase(@RequestHeader("Authorization") String token,
			@RequestBody CaseAssigDTO caseDto) throws UserNotFoundException, CaseNotFoundException {
		// Convert DTO to Entity
		CaseAssig caseRequest = modelMapper.map(caseDto, CaseAssig.class);

		CaseAssig caseAssig = caseService.createCase(token, caseRequest);
		// Entity to DTO
		CaseAssigDTO caseResponse = modelMapper.map(caseAssig, CaseAssigDTO.class);

		return new ResponseEntity<>(caseResponse, HttpStatus.CREATED);

	}

	/**
	 * Controller para eliminar casos
	 * 
	 * @url: /cases/{idCase}
	 * @param idCase: id del caso que se desea eliminar
	 * @throws CaseNotFoundException
	 */
	@ApiOperation(value = "Eliminacion de caso")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Caso eliminado"),
			@ApiResponse(code = 404, message = "Caso no encontrado") })
	@DeleteMapping("/{idCase}")
	public ResponseEntity<CaseAssig> deleteCase(@PathVariable Integer idCase,
			@RequestHeader("Authorization") String token) throws UserNotFoundException, CaseNotFoundException {
		// Llamado de servicio con los parametros necesarios para eliminar
		return caseService.deleteCase(idCase, token);
	}

	/**
	 * Controller Para asignar el caso de un usuario a otro
	 * 
	 * @url: /cases/{idCase}
	 * @param idCase:    id del caso a asignar
	 * @param caseDto: (RequestBody) estructura del caso(CaseAssig)
	 */

	@ApiOperation(value = "Asignacion de caso")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Caso asignado correctamente"),
			@ApiResponse(code = 404, message = "Caso no asignado") })
	@PutMapping
	public ResponseEntity<CaseAssigDTO> caseAssignment(@RequestParam Integer idCase, @RequestParam Long idUser,
			@RequestHeader("Authorization") String token, @RequestBody CaseAssigDTO caseDto)
			throws UserNotFoundException, CaseNotFoundException {
		// Convert DTO to Entity
		CaseAssig caseRequest = modelMapper.map(caseDto, CaseAssig.class);

		CaseAssig caseAssig = caseService.caseAssignment(token, idCase, idUser, caseRequest);
		// entity to DTO
		CaseAssigDTO caseResponse = modelMapper.map(caseAssig, CaseAssigDTO.class);
		return ResponseEntity.ok().body(caseResponse);

	}
	
	/**
	 * Controller para recahar casos
	 * @param idCase
	 * @param token
	 * @param caseDto
	 * @return
	 * @throws UserNotFoundException
	 * @throws CaseNotFoundException
	 */
	@ApiOperation(value = "Rechazo de caso")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Caso rechazado correctamente"),
			@ApiResponse(code = 404, message = "Caso no encontrado") })
	@PutMapping("/reject")
	public ResponseEntity<CaseAssigDTO> rejectCase(@RequestParam Integer idCase,@RequestHeader("Authorization") String token,
			@RequestBody CaseAssigDTO caseDto) throws UserNotFoundException, CaseNotFoundException {
		// Convert DTO to Entity
		CaseAssig caseRequest = modelMapper.map(caseDto, CaseAssig.class);

		CaseAssig caseAssig = caseService.rejectCase(idCase,token, caseRequest);
		// entity to DTO
		CaseAssigDTO caseResponse = modelMapper.map(caseAssig, CaseAssigDTO.class);
		return ResponseEntity.ok().body(caseResponse);
	}

}
