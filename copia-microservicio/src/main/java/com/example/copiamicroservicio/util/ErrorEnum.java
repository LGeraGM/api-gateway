package com.example.copiamicroservicio.util;

/**
 * @author Minsait
 * 
 *         The Enum ErrorEnum.
 * 
 *         Esta clase permite la enumeracion de diferentes mensajes de excepcion
 *         utilizados en los cuerpos de respuesta HTTP arrojados por el servicio
 * 
 * 
 */
public enum ErrorEnum {

	EXC_GENERICO("EXC.000", "Error generico", "Error generico de servidor", CollectionsConstantes.ERROR, ""),

	EXC_ERROR_PARAMS("EXC.001", "Parametros invalidos", "Parametros invalidos de consumo",
			CollectionsConstantes.WARNING, ""),

	EXC_OPER_NO_EXITOSA("EXC.102", "Operacion no exitosa", "Operacion no exitosa", CollectionsConstantes.ERROR, ""),

	EXC_OPER_CON_ERRORES("EXC.103", "Operacion con errores", "Operacion con errores", CollectionsConstantes.ERROR, ""),

	EXC_USER_NOT_FOUND("EXC.104", "Error de usuario", "Usuario no encontrado", CollectionsConstantes.ERROR, ""),
	EXC_CASE_NOT_FOUND("EXC.105", "Error de caso", "Caso no encontrado", CollectionsConstantes.ERROR, "");

	private final String code;

	private final String message;

	private final String description;

	private final String level;

	private final String moreInfo;

	ErrorEnum(final String code, final String message, final String description, final String level,
			final String moreInfo) {
		this.code = code;
		this.message = message;
		this.description = description;
		this.level = level;
		this.moreInfo = moreInfo;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String getDescription() {
		return description;
	}

	public String getLevel() {
		return level;
	}

	public String getMoreInfo() {
		return moreInfo;
	}

}
