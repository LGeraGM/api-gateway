/**
 * ********************************
 * Project: Case Management
 *
 * @author Gerardo Garcia
 * Empresa: Minsait
 * Fecha: 03/03/2022
 * TIPO: DTO
 * *********************************
 */
package com.example.copiamicroservicio.model;

import java.util.Date;

import lombok.Data;
/**
 * Minsait, Indra Mexico
 * @author Gerardo Garcia
 * Description: Clase DTO en la cual sus atributos puedean ser enviados y recuperados del servidor
 */
@Data
public class CaseAssigDTO {
	private Integer idCase;
	private String title;
	private String resumen;
	private String description;
	private Date date;
	private String status;
	private Long idUserO;
	private Long idUserN;
}
