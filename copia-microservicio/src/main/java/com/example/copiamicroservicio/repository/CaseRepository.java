/**
 * ********************************
 * Project: Case Management
 * @author Gerardo Garcia
 * Empresa: Minsait
 * Fecha: 21/01/20222
 * TIPO: Controller
 * *********************************
 */
package com.example.copiamicroservicio.repository;

import java.util.List;

import com.example.copiamicroservicio.entity.CaseAssig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Minsait Indra Mexico
 * Clase: CaseRepository.java
 * Descriptcion: Repositorio de los casos (CaseAssig) de la conexion con BD y JpaRepository.
 * Control de Cambios:
 * 1.0 Dic 21, 2022 : Gerardo - Creacion
 */
@Repository
public interface CaseRepository extends JpaRepository<CaseAssig, Integer>{
 List<CaseAssig> findByIdUserN(Long idUserN);
}
