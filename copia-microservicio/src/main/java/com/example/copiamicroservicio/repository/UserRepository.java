package com.example.copiamicroservicio.repository;

import java.util.Optional;

import com.example.copiamicroservicio.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
* Minsait
* Repositorio: UserRepository.java
* Descripción: Interfaz utilizada para validar el token que obtenemos del header "Authorization".
*/

public interface UserRepository extends JpaRepository<User,Long> {
	User findByUserName(String username);
	Optional<User> findById(Long userId);
   
}
