package com.example.copiamicroservicio.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* Minsait
* Entidad: User.java
* Descripción: Entidad utilizada para validar el token que obtenemos del header "Authorization".
* Uso de lombok para no especificar setters, getters y constructor
*/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "usuarios")
public class User {
    @Id
    private long id;
    
    private String userName;
    private String password;
    private String email;
}

