/**
 * ********************************
 * Project: Case Management
 *
 * @author Gerardo Garcia
 * Empresa: Minsait
 * Fecha: 21/01/20222
 * TIPO: Controller
 * *********************************
 */
package com.example.copiamicroservicio.entity;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Minsait Indra Mexico Clase: CaseAssig.java Descriptcion: Entidad de los casos
 * Control de Cambios: 1.0 Dic 21, 2022 Gerardo - Creacion
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "casos")
public class CaseAssig {
	// Atributos de los casos
	@Id
	@GeneratedValue
	private Integer idCase;
	
	private String title;
	private String resumen;
	private String description;
	private Date date;
	private String status;
	private Long idUserO;
	private Long idUserN;

}
