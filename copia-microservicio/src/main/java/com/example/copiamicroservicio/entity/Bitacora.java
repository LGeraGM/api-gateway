package com.example.copiamicroservicio.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Bitacora {

	@Id
	@GeneratedValue
	@Column(name="Id_bitacora")
	Integer id;
	
	@Column(name="Id_usuario_sesion")
	Long idUO;
	@Column(name="Accion_realizada")
	String action;
	@Column(name="Fecha_realizacion")
	Date date;
	
}
