package com.example.copiamicroservicio.service;

import java.util.List;

import com.example.copiamicroservicio.entity.User;

public interface IUserService {
List<User> getAllUsers(String token);
}
