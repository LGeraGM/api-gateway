package com.example.copiamicroservicio.service;

import java.util.ArrayList;

import com.example.copiamicroservicio.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.copiamicroservicio.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


/**
* Minsait
* Servicio: UserService.java
* Descripción: Servicio que se encarga de comunicarse con el repositorio y validar el token recibido en el header "Authorization".
*/

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUserName(username);
        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), new ArrayList<>());
    }
}
