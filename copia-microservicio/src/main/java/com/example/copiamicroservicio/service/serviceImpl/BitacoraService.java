package com.example.copiamicroservicio.service.serviceImpl;

import java.util.Date;

import com.example.copiamicroservicio.entity.Bitacora;
import com.example.copiamicroservicio.repository.BitacoraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BitacoraService {

	@Autowired
	private BitacoraRepository bitacoraReposotory;
	
	
	public void registroBitacora(Long idUser, String accion) {
		Bitacora newBitacora=new Bitacora();
		newBitacora.setId(null);
		newBitacora.setIdUO(idUser);
		newBitacora.setAction(accion);
		newBitacora.setDate(new Date());
		bitacoraReposotory.save(newBitacora);
	}
}
