/**
 * ********************************
 * Project: Case Management
 *
 * @author Gerardo Garcia
 * Empresa: Minsait
 * Fecha: 21/01/20222
 * TIPO: Controller
 * *********************************
 */
package com.example.copiamicroservicio.service.serviceImpl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.example.copiamicroservicio.config.security.JwtUtil;
import com.example.copiamicroservicio.entity.CaseAssig;
import com.example.copiamicroservicio.entity.User;
import com.example.copiamicroservicio.exception.CaseNotFoundException;
import com.example.copiamicroservicio.exception.UserNotFoundException;
import com.example.copiamicroservicio.repository.CaseRepository;
import com.example.copiamicroservicio.repository.UserRepository;
import com.example.copiamicroservicio.service.ICaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * Minsait Indra Mexico Clase: CaseService.java Descriptcion: Servicio de
 * respuesta al controller y comunicacion con la BD. Control de Cambios: 1.0 Dic
 * 21, 2022 Gerardo - Creacion
 */
@Slf4j
@Service
public class CaseService implements ICaseService {

	@Autowired
	private CaseRepository caseRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	JwtUtil jwtUtil;

	@Autowired
	private BitacoraService bitacora;

	/**
	 * Servicio de busqueda de casos especificos de un usuario
	 * 
	 * @param: Token, autenticacion de usuario
	 * @return: lista de objetos CaseAssig asignados al usuario(UserCase)
	 * @throws UserNotFoundException
	 */
	@Override
	public List<CaseAssig> getAllCasesByUser(String token) throws CaseNotFoundException {
		log.info("Extraccion del usuario del token");
		String tokenSub = token.substring(7);
		String userToken= jwtUtil.extractUsername(tokenSub);

		log.info("Busqueda del usuario en la BD");
		Optional<User> userCase = Optional.ofNullable(userRepository.findByUserName(userToken));
		log.info("Validacion de usuario existente");
		if (!userCase.isPresent()) {
			log.error("Usuario no encontrado en la BD");
			throw new CaseNotFoundException(userToken + " Usuario no encontrado");
		}
		User newUser=userCase.get();
		log.info("Registro en bitacora");
		bitacora.registroBitacora(newUser.getId(), "Consulta de casos");
		log.info("Retorno de la lista con los casos asignados al usuario");
		return caseRepository.findByIdUserN(newUser.getId());

	}

	/**
	 * Servicio de busqueda de caso especifico
	 * 
	 * @param token,  autenticacion del usuario
	 * @param idCase, ide del caso a consultar
	 * @throws CaseNotFoundException
	 */
	@Override
	public CaseAssig getCaseById(String token, Integer idCase)throws CaseNotFoundException {
		log.info("Extraccion del usuario del token");
		String tokenSub = token.substring(7);
		String tokenUser=jwtUtil.extractUsername(tokenSub);
		
		log.info("Busqueda de usuario y caso en la BD");
		Optional<User> userN=Optional.ofNullable(userRepository.findByUserName(tokenUser));
		Optional<CaseAssig> result = caseRepository.findById(idCase);
		
		log.info("Validacion de la existencia de caso y usuario en BD");
		if (result.isPresent() && userN.isPresent()) {
			log.info("Registro de consulta en bitacora");
			User userId=userN.get();
			bitacora.registroBitacora(userId.getId(), "Cosulta de caso: "+idCase);
			log.info("retorna el caso buscado");
			return result.get();
		} else {
			log.error("Caso y/o Usuario no encontrado");
			throw new CaseNotFoundException(idCase + "Caso no encontrado");
		}
		
	}

	/**
	 * Servicio de creacion de nuevos casos
	 * 
	 * @param: Token de autenticacion del usuario, objeto CaseAssig es cual
	 *               determina el cuerpo del caso
	 * @return: respuesta http created (ResponseEntity)
	 */
	@Override
	public CaseAssig createCase(String token, CaseAssig caseAssig) throws UserNotFoundException, CaseNotFoundException {
		log.info("Extraer los datos dal usuario que vienen en el token ");
		String tokenSub = token.substring(7);
		String userToken=jwtUtil.extractUsername(tokenSub);
	
		log.info("Consulta el usuario por el id obtenido del token ");
		Optional<User> userOptional = Optional.ofNullable(userRepository.findByUserName(userToken));
		if (!userOptional.isPresent()) {
			log.error("El usuario {} no se encontro", userToken);
			throw new UserNotFoundException("Usuario incorrecto");
		}
		caseAssig.setDate(new Date());
		caseAssig.setStatus("Registrado");
		log.info("extraxion de datos el usuario del resultado de la consulta");
		User newUserCase = userOptional.get();
		log.info("Asignacion de usuario en sesion al caso creado");
		caseAssig.setIdUserO(0L);
		caseAssig.setIdUserN(newUserCase.getId());
		log.info("Registro de creacion de caso en bitacora");
		User userId=userOptional.get();
		bitacora.registroBitacora(userId.getId(), "Creacion de nuevo caso");
		log.info("guardado del caso en la base de datos");
		return caseRepository.save(caseAssig);
	}

	/**
	 * Servicio de eliminacion de caso
	 * 
	 * @param: id del caso a eliminar
	 * @return: ResponseEntity ok
	 * @throws CaseNotFoundException
	 */
	@Override
	public ResponseEntity<CaseAssig> deleteCase(int idCase, String token) throws CaseNotFoundException {
		log.info("Extraccion de los datos del usuario del token");
		String tokenSub = token.substring(7);
		String tokenUser= jwtUtil.extractUsername(tokenSub);
		
		log.info("Busqueda de usuario en la BD");
		Optional<User> userOptional = Optional.ofNullable(userRepository.findByUserName(tokenUser));
		Optional<CaseAssig> caseOptional = caseRepository.findById(idCase);
		log.info("Validacion de exixstencia de usuario");
		if (!userOptional.isPresent() && !caseOptional.isPresent()) {
			log.error("Usuario no encontrado o valido");
			throw new CaseNotFoundException("Caso y/o Usuario no encontrado");
		}
		log.info("Eliminado del caso de la BD");
		caseRepository.deleteById(idCase);
		if(userOptional.isPresent()) {
			log.info("Registro de eliminacion de caso en bitacora");
			User userId=userOptional.get();
			bitacora.registroBitacora(userId.getId(), "Eliminacion de caso: " + idCase);	
		}
		
		log.info("Retorno de respuesta HTTP 200 ok");
		return ResponseEntity.ok().build();
	}

	/**
	 * Servicio de re-asignacion de casos entre usuarios
	 * 
	 * @param token      de autenticacion del usuario
	 * @param idCase:    id del caso a asignar
	 * @param caseAssig: ResponseEntity estructura del caso (CaseAssig)
	 * @return: ResponseEntity ok
	 * @throws CaseNotFoundException
	 */
	@Override
	public CaseAssig caseAssignment(String token, Integer idCase, Long idUser, CaseAssig caseAssig)
			throws CaseNotFoundException {
		log.info("Extraccion de informacion de usuario en sesion");
		String tokenSub = token.substring(7);
		String tokenUser=jwtUtil.extractUsername(tokenSub);
		
		log.info("Busqueda del caso en la BD");
		Optional<CaseAssig> caseOptional = caseRepository.findById(idCase);
		log.info("Busqueda de los usuarios en la BD");
		Optional<User> userReOptional = Optional.ofNullable(userRepository.findByUserName(tokenUser));
		Optional<User> userDes = userRepository.findById(idUser);
		log.info("Validadcion de que el caso y el usuario existan");
		if (caseOptional.isPresent() && userReOptional.isPresent() && userDes.isPresent()) {
			log.info("Guardado del caso en variables locales");
			CaseAssig newCAssig = caseOptional.get();

			log.info("Asignacion de los valores correspondientes al caso");
			User userT=userReOptional.get();
			caseAssig.setIdCase(newCAssig.getIdCase());
			caseAssig.setTitle(newCAssig.getTitle());
			caseAssig.setResumen(newCAssig.getResumen());
			caseAssig.setDescription(newCAssig.getDescription());
			caseAssig.setDate(new Date());
			caseAssig.setStatus("Asignado");
			caseAssig.setIdUserO(userT.getId());
			caseAssig.setIdUserN(idUser);

			log.info("Registro de asignacion de caso en bitacora");
			bitacora.registroBitacora(userT.getId(), ("Asignacion de caso: " + idCase + " al usuario: " + idUser));
		} else {
			log.error("Caso y/o usuario no encontrados");
			throw new CaseNotFoundException("Caso y/o Usuario no encontrado");
		}

		log.info("Guardado de los cambios en la BD");
		return caseRepository.save(caseAssig);

	}

	/**
	 * Servicio de caso rechazado
	 * 
	 * @param token
	 * @param caseAssig
	 * @return ResponseEntity ok
	 * @throws
	 */
	@Override
	public CaseAssig rejectCase(Integer idCase, String token, CaseAssig caseAssig) throws CaseNotFoundException {
		log.info("Extraccion de informacion del usuario en sesion");
		String tokenSub = token.substring(7);
		String tokenUser=jwtUtil.extractUsername(tokenSub);
		
		log.info("Busqueda de los usuarios en la BD");
		Optional<User> userOptional = Optional.ofNullable(userRepository.findByUserName(tokenUser));
		Optional<CaseAssig> caseOptional = caseRepository.findById(idCase);
		if (userOptional.isPresent() && caseOptional.isPresent()) {
			CaseAssig newCase = caseOptional.get();
			User userT=userOptional.get();
			caseAssig.setIdCase(newCase.getIdCase());
			caseAssig.setTitle(newCase.getTitle());
			caseAssig.setResumen(newCase.getResumen());
			caseAssig.setDescription(newCase.getDescription());
			caseAssig.setDate(new Date());
			caseAssig.setStatus("Re-Asignado");
			caseAssig.setIdUserO(userT.getId());
			caseAssig.setIdUserN(newCase.getIdUserO());
			log.info("Registro de rechazo de caso en bitacora");
			bitacora.registroBitacora(userT.getId(), ("Rechazo de caso: "+idCase+", asignado por usuario: "+newCase.getIdUserO()));
		} else {
			log.error("Caso y/o usuario no encontrados");
			throw new CaseNotFoundException("Caso y/o Usuario no encontrado");
		}
		log.info("Retorno de caso actualizado");
		return caseRepository.save(caseAssig);
	}
}
