package com.example.copiamicroservicio.service;

import java.util.List;

import com.example.copiamicroservicio.entity.CaseAssig;
import com.example.copiamicroservicio.exception.CaseNotFoundException;
import com.example.copiamicroservicio.exception.UserNotFoundException;
import org.springframework.http.ResponseEntity;

public interface ICaseService {

	/**
	 * getAllCasesByUser
	 * 
	 * @param token
	 * @return
	 * @throws UserNotFoundException
	 */
	List<CaseAssig> getAllCasesByUser(String token) throws UserNotFoundException;

	/**
	 * getCaseById
	 * @param token
	 * @param idCase
	 * @return
	 */
	CaseAssig getCaseById(String token, Integer idCase);
	
	/**
	 * createCase
	 * 
	 * @param token
	 * @param caseAssig
	 * @return
	 * @throws CaseNotFoundException
	 */
	CaseAssig createCase(String token, CaseAssig caseAssig) throws CaseNotFoundException;

	/**
	 * deleteCase
	 * 
	 * @param token
	 * @param idCase
	 * @return
	 */
	ResponseEntity<CaseAssig> deleteCase(int idCase, String token) throws CaseNotFoundException;

	/**
	 * caseAssignment
	 * 
	 * @param token
	 * @param idCase
	 * @param idUser
	 * @param caseAssig
	 * @return
	 * @throws CaseNotFoundException
	 */
	CaseAssig caseAssignment(String token, Integer idCase, Long idUser, CaseAssig caseAssig)
			throws CaseNotFoundException;

	/**
	 * rejectCase
	 * 
	 * @param idCase
	 * @param token
	 * @param caseAssig
	 * @return
	 * @throws CaseNotFoundException
	 */
	CaseAssig rejectCase(Integer idCase,String token, CaseAssig caseAssig)throws CaseNotFoundException;

}
